Source: broker
Priority: optional
Maintainer: Hilko Bengen <bengen@debian.org>
Build-Depends:
 debhelper (>= 12~),
 dh-python,
 cmake,
 libcaf-dev (>= 0.16),
 libsqlite3-dev,
 python3-dev,
 swig,
 libssl-dev,
 pybind11-dev,
Standards-Version: 4.3.0
Section: libs
Homepage: https://www.bro.org/
Vcs-Git: https://salsa.debian.org/bro-team/broker.git
Vcs-Browser: https://salsa.debian.org/bro-team/broker

Package: libbroker-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libbroker2 (= ${binary:Version}), ${misc:Depends}
Description: Bro's messaging library - development files
 The Broker library implements Bro's high-level communication
 patterns:
 .
  - remote logging
  - remote events
  - distributed data stores
 .
 This package contains header files.

Package: libbroker2
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Bro's messaging library
 The Broker library implements Bro's high-level communication
 patterns:
 .
  - remote logging
  - remote events
  - distributed data stores
 .
 This package contains the shared library.

Package: python3-broker
Architecture: any
X-Python3-Version: ${python3:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends},
Provides: ${python3:Provides},
Description: Bro's messaging library -- Python 3 bindings
 The Broker library implements Bro's high-level communication
 patterns:
 .
  - remote logging
  - remote events
  - distributed data stores
 .
 This package contains Python 3 bindings.
